package com.pv.mycv;

import android.content.Context;
import android.content.Intent;

import com.pv.mycv.ui.home.MainActivity;

/**
 * App navigation logic handled in this class
 */
public class AppNavigator {

    /**
     * starts the main activity
     * @param context context of the activity
     */
    public void startHomeActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }
}
