package com.pv.mycv;

import android.app.Application;

import com.pv.mycv.di.AppComponent;
import com.pv.mycv.di.AppModule;
import com.pv.mycv.di.DaggerAppComponent;
import com.pv.mycv.di.NetModule;

public class App extends Application {

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(getResources().getString(R.string.base_url)))
                .build();
    }

    public AppComponent getAppComponent(){
        return mAppComponent;
    }
}
