package com.pv.mycv.di;

import com.pv.mycv.ui.home.ExperienceDialogFragment;
import com.pv.mycv.ui.home.ExperienceFragment;
import com.pv.mycv.ui.home.ExtraFragment;
import com.pv.mycv.ui.home.MainActivity;
import com.pv.mycv.ui.home.MainPresenter;
import com.pv.mycv.ui.home.ProfileFragment;
import com.pv.mycv.ui.home.SkillsetFragment;
import com.pv.mycv.ui.welcome.SplashActivity;
import com.pv.mycv.ui.welcome.SplashPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface AppComponent {

    void inject(SplashActivity splashActivity);
    void inject(SplashPresenter presenter);

    void inject(MainActivity mainActivity);
    void inject(MainPresenter presenter);

    void inject(ExperienceFragment fragment);
    void inject(SkillsetFragment fragment);
    void inject(ExtraFragment fragment);
    void inject(ProfileFragment fragment);
    void inject(ExperienceDialogFragment fragment);
}
