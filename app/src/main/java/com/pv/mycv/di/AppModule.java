package com.pv.mycv.di;

import android.app.Application;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.pv.mycv.AppNavigator;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * App module class provides dependencies to the other classes
 */
@Module
public class AppModule {

    Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    public JacksonConverterFactory provideJsonConverterFactory() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_ENUMS_USING_TO_STRING, true);
        mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
        return JacksonConverterFactory.create(mapper);
    }

    @Provides
    @Named("main")
    public Scheduler provideMainThreadScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @Named("background")
    public Scheduler provideBackgroundScheduler() {
        return Schedulers.io();
    }

    @Provides
    @Singleton
    public AppNavigator provideAppNavigator(){
        return new AppNavigator();
    }

    /**
     * Returns the CV response object
     * When the app loads up this object is initialised with the response from the server
     * and then on only read from the fragment (so no synchronisation required)
     * @return instance of a cv response
     */
    @Provides
    @Singleton
    public CVResponseProvider provideCVResponseProvider() {
        return new CVResponseProvider();
    }
}