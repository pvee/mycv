package com.pv.mycv.di;

import com.pv.mycv.model.CVResponse;

/**
 *  Helper class (for dependency injection) cv response
 */
public class CVResponseProvider {
    private CVResponse response;

    public CVResponse getResponse() {
        return response;
    }

    public void setResponse(CVResponse response) {
        this.response = response;
    }
}
