package com.pv.mycv.base;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import butterknife.Unbinder;

/**
 *  Base activity of all activities initialises the presenter
 */
public abstract class BaseActivity<B extends BasePresenter> extends AppCompatActivity implements BaseView {
    public B presenter;
    protected Unbinder unbinder;

    /**
     *  Method to make sure the dependencies are initialised
     *  eg: we can create presenter, inject dependencies etc
     */
    protected abstract void setupDependencies();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // currently we support portrait mode only due to the time constraints
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setupDependencies();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @Override
    public Context getContext() {
        return this;
    }
}
