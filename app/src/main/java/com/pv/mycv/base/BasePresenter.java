package com.pv.mycv.base;

public abstract class BasePresenter {

    protected void onStart() {

    }

    protected void onResume() {

    }

    protected void onPause() {

    }

    protected void onStop() {

    }
}
