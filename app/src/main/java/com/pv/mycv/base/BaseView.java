package com.pv.mycv.base;

import android.content.Context;

/**
 *  Base view interface for communicating between presenter and the activity
 */
public interface BaseView {
    Context getContext();
}
