package com.pv.mycv.ui.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pv.mycv.R
import com.pv.mycv.model.Skillset
import kotlinx.android.synthetic.main.additional_info_item.view.*

/**
 * Adapter class for showing skillset info. it uses add info item xml 
 * because both data are similar at the moment
 */
class SkillsetAdapter(val items: List<Skillset>, val context: Context) : RecyclerView.Adapter<SkillsetViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SkillsetViewHolder {
        return SkillsetViewHolder(LayoutInflater.from(context).inflate(R.layout.additional_info_item, parent, false))
    }

    @SuppressLint("StringFormatMatches")
    override fun onBindViewHolder(holder: SkillsetViewHolder, position: Int) {
        holder?.tvDetails.text = items.get(position).details
        holder?.tvTitle.text = items.get(position).technology
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class SkillsetViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val tvTitle = view.textViewTitle
    val tvDetails = view.textViewDetails
}
