package com.pv.mycv.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.pv.mycv.App
import com.pv.mycv.R
import com.pv.mycv.di.CVResponseProvider
import com.pv.mycv.model.CVResponse
import javax.inject.Inject

/**
 *  Fragment to display the profile information from the CV
 */
class ProfileFragment : Fragment() {

    @Inject
    lateinit var cvResponseProvider: CVResponseProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val appComponent = (activity?.application as App).getAppComponent()
        appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view : View = inflater.inflate(R.layout.fragment_profile, container, false)
        setValues(view)
        return view;
    }

    /**
     *  set values
     */
    private fun setValues(view: View) {
        val cvResponse : CVResponse = cvResponseProvider.response;
        view.findViewById<TextView>(R.id.textViewName).text = cvResponse.personalInfo.name
        view.findViewById<TextView>(R.id.textViewProfile).text = cvResponse.profile
        view.findViewById<TextView>(R.id.textViewEmail).text = cvResponse.personalInfo.email
        view.findViewById<TextView>(R.id.textViewMobile).text = cvResponse.personalInfo.phoneMob
        view.findViewById<TextView>(R.id.textViewHome).text = cvResponse.personalInfo.phoneHome
        view.findViewById<TextView>(R.id.textViewWebsite).text = cvResponse.personalInfo.web
        view.findViewById<TextView>(R.id.textViewGitInfo).text = cvResponse.personalInfo.git
    }


    companion object {
        /**
         * Use this factory method to create a new instance of this fragment
         */
        @JvmStatic
        fun newInstance() = ProfileFragment()
    }
}
