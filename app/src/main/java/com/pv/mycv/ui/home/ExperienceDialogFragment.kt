package com.pv.mycv.ui.home

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.pv.mycv.App
import com.pv.mycv.R
import com.pv.mycv.di.CVResponseProvider
import javax.inject.Inject

const val EXP_ITEM_POS = "exp_item_pos"

/**
 *
 * You activity (or fragment) needs to implement [ExperienceDialogFragment.Listener].
 */
class ExperienceDialogFragment : BottomSheetDialogFragment() {

    @Inject
    lateinit var cvResponseProvider: CVResponseProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val appComponent = (activity?.application as App).getAppComponent()
        appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.experience_detail, container, false)
        val pos = arguments?.getInt(EXP_ITEM_POS)
        pos?.let { setValues(view, it) }
        return view
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        dialog.setOnShowListener {
            val bottomSheet = dialog.findViewById<View>(
                    com.google.android.material.R.id.design_bottom_sheet) as? FrameLayout
            val behavior = BottomSheetBehavior.from(bottomSheet)
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        return dialog
    }

    private fun setValues(view: View, pos : Int) {
        val experience = cvResponseProvider.response.experiences[pos]
        view.findViewById<TextView>(R.id.textViewJobTitle).text = experience.jobTitle
        view.findViewById<TextView>(R.id.textViewJobType).text = if (experience.jobType == 0) getString(R.string.permanent) else
            getString(R.string.contractor)
        view.findViewById<TextView>(R.id.textViewDuration).text = getString(R.string.from_to, experience.dateFrom, experience.dateTo)
        view.findViewById<TextView>(R.id.textViewCompany).text = experience.company
        view.findViewById<TextView>(R.id.textViewCompWebsite).text = experience.website
        view.findViewById<TextView>(R.id.textViewTechs).text = experience.techUsed
        view.findViewById<TextView>(R.id.textViewDetails).text = experience.details
        view.findViewById<Button>(R.id.buttonClose).transformationMethod = null
        view.findViewById<Button>(R.id.buttonClose).setOnClickListener{
            this.dismiss()
        }

    }

    companion object {
        fun newInstance(expItemPos: Int): ExperienceDialogFragment =
            ExperienceDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(EXP_ITEM_POS, expItemPos)
                }
            }
    }
}
