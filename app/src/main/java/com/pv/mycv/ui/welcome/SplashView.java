package com.pv.mycv.ui.welcome;

import com.pv.mycv.base.BaseView;

public interface SplashView extends BaseView {
    void showProgress();

    void showError();

    void finishActivity();
}
