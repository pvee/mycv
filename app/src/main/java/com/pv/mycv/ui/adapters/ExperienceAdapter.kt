package com.pv.mycv.ui.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pv.mycv.R
import com.pv.mycv.model.Experience
import kotlinx.android.synthetic.main.experience_item.view.*

/**
 * Adapter class to list all the experiences
 */
class ExperienceAdapter(val items: List<Experience>, val context: Context) : RecyclerView.Adapter<ExperienceAdapter.ExpViewHolder>() {

    var onItemClick: ((Int) -> Unit)? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExpViewHolder {
        return ExpViewHolder(LayoutInflater.from(context).inflate(R.layout.experience_item, parent, false))
    }

    @SuppressLint("StringFormatMatches")
    override fun onBindViewHolder(holder: ExpViewHolder, position: Int) {
        holder?.tvCompany?.text = items.get(position).company
        holder?.tvTechs.text = items.get(position).techUsed
        holder?.tvJobTitle.text = items.get(position).jobTitle
        holder?.tvJobType.text = if (items.get(position).jobType == 1)
            context.getString(R.string.contractor) else context.getString(R.string.permanent)
        holder?.tvDuration?.text = context.getString(R.string.from_to, items.get(position).dateFrom, items.get(position).dateTo)
        holder?.ivDetail?.setImageResource(R.drawable.arrow_right_24px)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ExpViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvJobTitle = view.textViewJobTitle
        val tvJobType = view.textViewJobType
        val tvDuration = view.textViewDuration
        val tvCompany = view.textViewCompany
        val tvTechs = view.textViewTechs
        val ivDetail = view.imageViewDetail

        init {
            view.setOnClickListener{
                onItemClick?.invoke(adapterPosition)
            }
        }
    }
}


