package com.pv.mycv.ui.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pv.mycv.App
import com.pv.mycv.R
import com.pv.mycv.di.CVResponseProvider
import com.pv.mycv.ui.adapters.ExperienceAdapter
import javax.inject.Inject

/**
 *  Fragment to the experiences form  the CV in a list format (recent on top
 */
class ExperienceFragment : Fragment() {

    @Inject
    lateinit var cvResponseProvider: CVResponseProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val appComponent = (activity?.application as App).getAppComponent()
        appComponent.inject(this)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view : View = inflater.inflate(R.layout.fragment_experience, container, false)

        val expList: RecyclerView = view.findViewById<RecyclerView>(R.id.listExperience)
        expList?.layoutManager = LinearLayoutManager(activity)
        val experienceAdapter = ExperienceAdapter(cvResponseProvider.response.experiences, activity!!.applicationContext)
        expList?.adapter = experienceAdapter
        experienceAdapter.onItemClick = {position ->
            ExperienceDialogFragment.newInstance(position).show(fragmentManager, null)
        }

        return view;
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @return A new instance of fragment ExperienceFragment.
         */
        @JvmStatic
        fun newInstance() = ExperienceFragment()
    }
}
