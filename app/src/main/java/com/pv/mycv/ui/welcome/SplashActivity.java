package com.pv.mycv.ui.welcome;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pv.mycv.App;
import com.pv.mycv.R;
import com.pv.mycv.base.BaseActivity;
import com.pv.mycv.di.AppComponent;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashActivity extends BaseActivity<SplashPresenter> implements SplashView {

    @BindView(R.id.progressBarLoading)
    ProgressBar progressBarLoading;
    @BindView(R.id.textViewLoadingInfo)
    TextView textViewLoadingInfo;
    @BindView(R.id.buttonRetry)
    Button buttonRetry;

    /**
     * init the presenter and inject dependencies to the activity and presenter
     */
    @Override
    protected void setupDependencies() {
        AppComponent appComponent = ((App) getApplication()).getAppComponent();
        appComponent.inject(this);
        presenter = new SplashPresenter(this);
        appComponent.inject(presenter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        buttonRetry.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        textViewLoadingInfo.setText(R.string.loading_msg);
        buttonRetry.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        textViewLoadingInfo.setText(R.string.loading_err);
        buttonRetry.setVisibility(View.VISIBLE);
    }

    @Override
    public void finishActivity() {
        SplashActivity.this.finish();
    }

    @OnClick({R.id.buttonRetry})
    public void onRetry() {
        presenter.getCV();
    }
}
