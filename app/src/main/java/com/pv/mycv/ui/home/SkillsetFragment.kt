package com.pv.mycv.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pv.mycv.App
import com.pv.mycv.R
import com.pv.mycv.di.CVResponseProvider
import com.pv.mycv.ui.adapters.SkillsetAdapter
import javax.inject.Inject

/**
 *  The fragment responsible for showing the list of skills from the CV
 */
class SkillsetFragment : Fragment() {

    @Inject
    lateinit var cvResponseProvider: CVResponseProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val appComponent = (activity?.application as App).getAppComponent()
        appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view : View = inflater.inflate(R.layout.fragment_skillset, container, false)
        val skillsetList: RecyclerView = view.findViewById<RecyclerView>(R.id.listSkillset)
        skillsetList?.layoutManager = LinearLayoutManager(activity)
        skillsetList?.adapter = SkillsetAdapter(cvResponseProvider.response.skillset, activity!!.applicationContext)
        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @return A new instance of fragment SkillsetFragment.
         */
        @JvmStatic
        fun newInstance() = SkillsetFragment()
    }
}
