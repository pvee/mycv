package com.pv.mycv.ui.home;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.pv.mycv.App;
import com.pv.mycv.R;
import com.pv.mycv.base.BaseActivity;
import com.pv.mycv.di.AppComponent;

import butterknife.ButterKnife;

/**
 * Main activity to display CV fragments. fragments can be switched between using the bottom nav
 */
public class MainActivity extends BaseActivity<MainPresenter> implements MainView {

    private Fragment selectedFragment;

    //todo: instead of creating every time we switch - add to backstack or use a list and replace the fragments
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_profile:
                    selectedFragment = ProfileFragment.newInstance();
                    loadFragment();
                    return true;
                case R.id.navigation_experience:
                    selectedFragment = ExperienceFragment.newInstance();
                    loadFragment();
                    return true;
                case R.id.navigation_skillset:
                    selectedFragment = SkillsetFragment.newInstance();
                    loadFragment();
                    return true;
                case R.id.navigation_add_info:
                    selectedFragment = ExtraFragment.newInstance();
                    loadFragment();
                    return true;
            }
            return false;
        }
    };


    @Override
    protected void setupDependencies() {
        AppComponent appComponent = ((App) getApplication()).getAppComponent();
        appComponent.inject(this);
        presenter = new MainPresenter(this);
        appComponent.inject(presenter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navView.setSelectedItemId(R.id.navigation_profile);
    }

    private void loadFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragContent, selectedFragment).commit();
    }

}
