package com.pv.mycv.ui.welcome;

import com.pv.mycv.AppNavigator;
import com.pv.mycv.api.ApiManagerService;
import com.pv.mycv.base.BasePresenter;
import com.pv.mycv.di.CVResponseProvider;
import com.pv.mycv.model.CVResponse;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * splash view presenter class
 * this classed is used to get the CV data from the server and cache it
 */
public class SplashPresenter extends BasePresenter {
    private SplashView mView;

    private final String TAG = "SplashPresenter";
    @Inject
    @Named("main")
    public
    io.reactivex.Scheduler mMainThreadScheduler;
    @Inject
    @Named("background")
    public
    io.reactivex.Scheduler mBackgroundThreadScheduler;
    @Inject
    public
    ApiManagerService mApiManagerService;
    @Inject
    public
    AppNavigator mAppNavigator;
    @Inject
    public
    CVResponseProvider cvResponseProvider;

    private Disposable mDisposable;

    public SplashPresenter(SplashView view) {
        this.mView = view;
    }

    @Override
    protected void onStart() {
        super.onStart();
        getCV();
    }

    /**
     * method to retrieve the CV from the server
     */
    public void getCV() {
        mDisposable = mApiManagerService.getCV()
                .observeOn(mMainThreadScheduler)
                .subscribeOn(mBackgroundThreadScheduler)
                .doOnSubscribe(disposable -> mView.showProgress())
                .subscribeWith(new DisposableSingleObserver<CVResponse>() {
                    @Override
                    public void onSuccess(CVResponse response) {
                        cacheCVResponse(response);
                    }

                    @Override
                    public void onError(Throwable e) {
//                      Log.d(TAG, "error in cv response " + e.getMessage());
                        mView.showError();
                    }
                });
    }

    private void cacheCVResponse(CVResponse response) {
        cvResponseProvider.setResponse(response);
        mView.finishActivity();
        mAppNavigator.startHomeActivity(mView.getContext());
    }

    @Override
    protected void onStop() {
        if (mDisposable != null) mDisposable.dispose();
        super.onStop();
    }
}
