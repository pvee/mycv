package com.pv.mycv.ui.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pv.mycv.R
import com.pv.mycv.model.AdditionalInfo
import kotlinx.android.synthetic.main.additional_info_item.view.*

/**
 * Adapter class for showing additional info
 */
class AddInfoAdapter(val items: List<AdditionalInfo>, val context: Context) : RecyclerView.Adapter<AddInfoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddInfoViewHolder {
        return AddInfoViewHolder(LayoutInflater.from(context).inflate(R.layout.additional_info_item, parent, false))
    }

    @SuppressLint("StringFormatMatches")
    override fun onBindViewHolder(holder: AddInfoViewHolder, position: Int) {
        holder?.tvDetails.text = items.get(position).details
        holder?.tvTitle.text = items.get(position).title
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class AddInfoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val tvTitle = view.textViewTitle
    val tvDetails = view.textViewDetails
}
