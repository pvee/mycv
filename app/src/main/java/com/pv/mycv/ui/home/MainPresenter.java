package com.pv.mycv.ui.home;

import com.pv.mycv.base.BasePresenter;

/**
 * Main activity presenter class.
 */
public class MainPresenter extends BasePresenter {

    private final MainView mView;

    public MainPresenter(MainView view) {
        this.mView = view;
    }
}
