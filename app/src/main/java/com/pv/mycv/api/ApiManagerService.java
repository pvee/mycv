package com.pv.mycv.api;

import com.pv.mycv.model.CVResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface ApiManagerService {
    @GET(".")
    @Headers({
            "secret-key:$2a$10$L/jiTa7BN3bTwlencCWr4uoPYVOX/uYpTliMQOmTIfejTDrCZ/qry"
    })
    Single<CVResponse> getCV();
}
