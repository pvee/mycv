package com.pv.mycv.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Skillset {

    @JsonProperty("technology")
    private String technology;
    @JsonProperty("details")
    private String details;
    @JsonProperty("main_tech")
    private Boolean mainTech;
    @JsonProperty("group_id")
    private Integer groupId;

    @JsonProperty("technology")
    public String getTechnology() {
        return technology;
    }

    @JsonProperty("technology")
    public void setTechnology(String technology) {
        this.technology = technology;
    }

    @JsonProperty("details")
    public String getDetails() {
        return details;
    }

    @JsonProperty("details")
    public void setDetails(String details) {
        this.details = details;
    }

    @JsonProperty("main_tech")
    public Boolean getMainTech() {
        return mainTech;
    }

    @JsonProperty("main_tech")
    public void setMainTech(Boolean mainTech) {
        this.mainTech = mainTech;
    }

    @JsonProperty("group_id")
    public Integer getGroupId() {
        return groupId;
    }

    @JsonProperty("group_id")
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

}