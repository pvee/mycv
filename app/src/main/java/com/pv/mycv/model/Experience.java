package com.pv.mycv.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Experience {
    @JsonProperty("job_title")
    private String jobTitle;
    @JsonProperty("job_type")
    private Integer jobType;
    @JsonProperty("company")
    private String company;
    @JsonProperty("website")
    private String website;
    @JsonProperty("date_from")
    private String dateFrom;
    @JsonProperty("date_to")
    private String dateTo;
    @JsonProperty("details")
    private String details;
    @JsonProperty("tech_used")
    private String techUsed;

    @JsonProperty("job_title")
    public String getJobTitle() {
        return jobTitle;
    }

    @JsonProperty("job_title")
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    @JsonProperty("job_type")
    public Integer getJobType() {
        return jobType;
    }

    @JsonProperty("job_type")
    public void setJobType(Integer jobType) {
        this.jobType = jobType;
    }

    @JsonProperty("company")
    public String getCompany() {
        return company;
    }

    @JsonProperty("company")
    public void setCompany(String company) {
        this.company = company;
    }

    @JsonProperty("website")
    public String getWebsite() {
        return website;
    }

    @JsonProperty("website")
    public void setWebsite(String website) {
        this.website = website;
    }

    @JsonProperty("date_from")
    public String getDateFrom() {
        return dateFrom;
    }

    @JsonProperty("date_from")
    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    @JsonProperty("date_to")
    public String getDateTo() {
        return dateTo;
    }

    @JsonProperty("date_to")
    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    @JsonProperty("details")
    public String getDetails() {
        return details;
    }

    @JsonProperty("details")
    public void setDetails(String details) {
        this.details = details;
    }

    @JsonProperty("tech_used")
    public String getTechUsed() {
        return techUsed;
    }

    @JsonProperty("tech_used")
    public void setTechUsed(String techUsed) {
        this.techUsed = techUsed;
    }

}
