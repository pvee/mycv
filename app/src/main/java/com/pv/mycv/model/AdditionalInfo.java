package com.pv.mycv.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *  class to store any additional information just like hobbies, prof certificates etc
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdditionalInfo {

    @JsonProperty("title")
    private String title;

    @JsonProperty("details")
    private String details;

    @JsonProperty("details")
    public String getDetails() {
        return details;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("details")
    public void setDetails(String details) {
        this.details = details;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }
}
