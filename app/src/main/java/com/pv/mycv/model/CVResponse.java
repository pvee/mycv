package com.pv.mycv.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Model class to store a CV
 * currently we store only one CV for the demo, otherwise store these objects in an array
 */
public class CVResponse {

    @JsonProperty("cv_name")
    private String cvName;
    @JsonProperty("date_modified")
    private String dateModified;
    @JsonProperty("cv_desc")
    private String cvDesc;
    @JsonProperty("profile_short")
    private String profileShort;
    @JsonProperty("profile")
    private String profile;
    @JsonProperty("personalInfo")
    private PersonalInfo personalInfo;
    @JsonProperty("experiences")
    private List<Experience> experiences = null;
    @JsonProperty("skillset")
    private List<Skillset> skillset = null;
    @JsonProperty("additional_info")
    private List<AdditionalInfo> additionalInfo = null;

    @JsonProperty("cv_name")
    public String getCvName() {
        return cvName;
    }

    @JsonProperty("cv_name")
    public void setCvName(String cvName) {
        this.cvName = cvName;
    }

    @JsonProperty("date_modified")
    public String getDateModified() {
        return dateModified;
    }

    @JsonProperty("date_modified")
    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    @JsonProperty("cv_desc")
    public String getCvDesc() {
        return cvDesc;
    }

    @JsonProperty("cv_desc")
    public void setCvDesc(String cvDesc) {
        this.cvDesc = cvDesc;
    }

    @JsonProperty("profile_short")
    public String getProfileShort() {
        return profileShort;
    }

    @JsonProperty("profile_short")
    public void setProfileShort(String profileShort) {
        this.profileShort = profileShort;
    }

    @JsonProperty("profile")
    public String getProfile() {
        return profile;
    }

    @JsonProperty("profile")
    public void setProfile(String profile) {
        this.profile = profile;
    }

    @JsonProperty("personalInfo")
    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    @JsonProperty("personalInfo")
    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    @JsonProperty("experiences")
    public List<Experience> getExperiences() {
        return experiences;
    }

    @JsonProperty("experiences")
    public void setExperiences(List<Experience> experiences) {
        this.experiences = experiences;
    }

    @JsonProperty("skillset")
    public List<Skillset> getSkillset() {
        return skillset;
    }

    @JsonProperty("skillset")
    public void setSkillset(List<Skillset> skillset) {
        this.skillset = skillset;
    }

    @JsonProperty("additional_info")
    public List<AdditionalInfo> getAdditionalInfo() {
        return additionalInfo;
    }

    @JsonProperty("additional_info")
    public void setAdditionalInfo(List<AdditionalInfo> additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

}

