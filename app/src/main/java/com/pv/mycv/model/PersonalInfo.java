package com.pv.mycv.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PersonalInfo {

    @JsonProperty("name")
    private String name;
    @JsonProperty("dob")
    private String dob;
    @JsonProperty("phone_home")
    private String phoneHome;
    @JsonProperty("phone_mob")
    private String phoneMob;
    @JsonProperty("email")
    private String email;
    @JsonProperty("web")
    private String web;
    @JsonProperty("git")
    private String git;
    @JsonProperty("photo")
    private String photo;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("dob")
    public String getDob() {
        return dob;
    }

    @JsonProperty("dob")
    public void setDob(String dob) {
        this.dob = dob;
    }

    @JsonProperty("phone_home")
    public String getPhoneHome() {
        return phoneHome.replaceAll("\\s+", "");
    }

    @JsonProperty("phone_home")
    public void setPhoneHome(String phoneHome) {
        this.phoneHome = phoneHome;
    }

    @JsonProperty("phone_mob")
    public String getPhoneMob() {
        return phoneMob.replaceAll("\\s+", "");
    }

    @JsonProperty("phone_mob")
    public void setPhoneMob(String phoneMob) {
        this.phoneMob = phoneMob;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("web")
    public String getWeb() {
        return web;
    }

    @JsonProperty("web")
    public void setWeb(String web) {
        this.web = web;
    }

    @JsonProperty("git")
    public String getGit() {
        return git;
    }

    @JsonProperty("git")
    public void setGit(String git) {
        this.git = git;
    }

    @JsonProperty("photo")
    public String getPhoto() {
        return photo;
    }

    @JsonProperty("photo")
    public void setPhoto(String photo) {
        this.photo = photo;
    }

}
