package com.pv.mycv.ui;

import android.content.Context;

import com.pv.mycv.AppNavigator;
import com.pv.mycv.api.ApiManagerService;
import com.pv.mycv.di.CVResponseProvider;
import com.pv.mycv.model.CVResponse;
import com.pv.mycv.ui.welcome.SplashPresenter;
import com.pv.mycv.ui.welcome.SplashView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SplashPresenterTest {

    private SplashPresenter presenter;

    @Mock
    protected Context context;

    @Mock
    protected ApiManagerService service;

    @Mock
    AppNavigator appNavigator;

    @Mock
    SplashView splashView;

    @Mock
    CVResponseProvider cvResponseProvider;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        presenter = new SplashPresenter(splashView);
        presenter.mApiManagerService = service;
        presenter.mAppNavigator = appNavigator;
        cvResponseProvider = new CVResponseProvider();
        presenter.cvResponseProvider = cvResponseProvider;
        presenter.mBackgroundThreadScheduler =  Schedulers.io();
        presenter.mMainThreadScheduler = Schedulers.io();
        when(splashView.getContext()).thenReturn(context);
    }

    @Test
    public void onStartOK() {
        when(service.getCV()).thenReturn(Single.just(getCVResponse()));
        presenter.getCV();
        verify(splashView).showProgress();
        verify(appNavigator).startHomeActivity(context);
        assertTrue(cvResponseProvider.getResponse().getCvName().equals("test cv"));
    }

    @Test
    public void onStartFail() {
        when(service.getCV()).thenReturn(Single.error(new Throwable("Error")));
        presenter.getCV();
        verify(splashView).showProgress();
        verify(splashView).showError();
    }

    private CVResponse getCVResponse(){
        CVResponse response = new CVResponse();
        response.setCvName("test cv");

        return response;
    }
}